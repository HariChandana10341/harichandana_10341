package com.srpingdemo.Main;

import com.srpingdemo.bean.Address;
import com.srpingdemo.bean.Employee;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		

		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:com/springdemo/config/Context.xml");
	   Employee emp=(Employee) context.getBean("emp");
	   
	   
	   System.out.println(emp.getEmpId());
	   System.out.println(emp.getEmpName());
	   Address empAddress =emp.getAddress();
	   System.out.println(empAddress.getCity());
	   System.out.println(empAddress.getState());
	   

	}

}
