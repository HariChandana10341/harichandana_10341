package com.abc.javaconfig;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * This class is used to establish Spring configuration that implements WebMvcConfigurer. 
 * @author CHANDANA
 *
 */
@EnableWebMvc

@Configuration
@ComponentScan(basePackages = "com.abc")
public class SpringConfiguration implements WebMvcConfigurer {

	/*
	 * enables CORS requests from any origin to any endpoint in the application.
	 */
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**");
		
	}


}
