package com.abc.javaconfig;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * This class is used to establish WebServletConfiguration . 
 * @author CHANDANA
 *
 */
public class WebServletConfiguration implements WebApplicationInitializer {

	@Override
	public void onStartup(final ServletContext sevletContext) throws ServletException {

		AnnotationConfigWebApplicationContext webContext = new AnnotationConfigWebApplicationContext();
		webContext.register(SpringConfiguration.class);
		webContext.setServletContext(sevletContext);
		DispatcherServlet dispatcherServlet = new DispatcherServlet(webContext);
		// Create a servlet dynamically.
		ServletRegistration.Dynamic servlet = sevletContext.addServlet("dispatcherServlet", dispatcherServlet);
		servlet.setLoadOnStartup(1);
		servlet.addMapping("/");
	
	}

}
