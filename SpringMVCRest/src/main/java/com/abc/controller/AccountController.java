package com.abc.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.abc.hibernate.entities.Account;
import com.abc.service.AccountService;


/**
 * This class acts as a controller which calls all the service methods and returns response
 * @author CHANDANA
 *
 */
@RestController
public class AccountController {

	//creating the object for account service
	@Autowired
	private AccountService accountService;
	
	/**
	 * This  method  is used to search account by ID
	 * @param accno
	 * @return ResponseEntity
	 */
	@GetMapping("accountsearch/{id}")
	public ResponseEntity<Account> searchAccount(@PathVariable("id") int accno) 
	{
		Account account = accountService.searchAccountByID(accno);
		if(account==null)
		{
			return new ResponseEntity<Account>(HttpStatus.NO_CONTENT);		
		}
		else
		{
			return new ResponseEntity<Account>(account,HttpStatus.OK);
		}
		
	}
	
	/**
	 * This method is used to insert account details
	 * @param account
	 * @return ResponseEntity
	 */
	@PostMapping("accountsinsertion")
	public ResponseEntity<?> insertAccount(@RequestBody Account account) 
	{
		return ResponseEntity.ok().body("Account Saved.\nYour Account Number is:"+accountService.insertAccount(account));		
	}
	
	
	/**
	 * This  method  is used to delete account by ID
	 * @param accno
	 * @return ResponseEntity
	 */
	@DeleteMapping("accountsdelete/{id}")
	public ResponseEntity<?> deleteAccount(@PathVariable("id") int accno) 
	{
		if(accountService.deleteAccount(accno))
		{
			return ResponseEntity.ok().body("Account Deleted");
		}
		else
		{
			return (ResponseEntity<?>) ResponseEntity.status(HttpStatus.NOT_MODIFIED);
		}
		
	}
	
	/**
	 * This method is used to update account details
	 * @param account
	 * @return ResponseEntity
	 */
	@PutMapping("accountUpdate")
	public ResponseEntity<?> updateAccount(@RequestBody Account account) {		
		
		if(accountService.updateAccount(account)) {	
			
			
				
			return ResponseEntity.ok().body("Account Updated");
			
		}
		else {
			return (ResponseEntity<?>) ResponseEntity.status(HttpStatus.NOT_MODIFIED);
		}
	}
	
	/**
	 * This method is used to display all the accounts inside a database
	 * @return ResponseEntity
	 */
	@GetMapping("getAllAccounts")
	public ResponseEntity<List<Account>> getAllAccounts()
	{
		List<Account> accounts = accountService.getAllAccounts();
		if(accounts.size()>0)
		{
			return new ResponseEntity<List<Account>>(accounts,HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity<List<Account>>(HttpStatus.NO_CONTENT);
		}
		
	}
}








