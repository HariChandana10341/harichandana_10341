package com.abc.dao;

import java.util.List;

import com.abc.hibernate.entities.Account;

/**
 * This class acts as an interface to accountDAO class which has all the abstract methods of CRUD operations
 * @author CHANDANA
 *
 */
public interface AccountDAO {

	Account getAccountByID(int accno);
	
	int insertAccount(Account account);
	
	boolean deleteAccount(int accno);
	
	boolean updateAccount(Account account);
	
	List<Account> getAllAccounts();
}
