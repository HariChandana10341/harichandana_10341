package com.abc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.abc.hibernate.entities.Account;

/**
 * This class is used to establish session and performs CRUD operations
 * @author CHANDANA
 *
 */
@Repository
public class AccountDAOImpl implements AccountDAO {
	
	//creating object for session factory
	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * This  method  is used to search account by ID
	 * @param accno
	 * @return account
	 */
	@Override
	public Account getAccountByID(int accno) {
		Session session = sessionFactory.getCurrentSession();
		Account account = session.get(Account.class, accno);
		return account;
	}

	/**
	 * This method is used to insert account details
	 * @param account
	 * @return account number
	 */
	@Override
	public int insertAccount(Account account) {
		Session session = sessionFactory.getCurrentSession();
		session.save(account);
		return account.getAccno();
	}

	/**
	 * This  method  is used to delete account by ID
	 * @param accno
	 * @return true if deleted successfully
	 */
	@Override
	public boolean deleteAccount(int accno) {
		Session session = sessionFactory.getCurrentSession();
		Account account = session.get(Account.class, accno);
		if(account==null)
		{
			return false;
		}
		else
		{
			session.delete(account);
			return true;
		}
	}

	/**
	 * This method is used to update account details
	 * @param account
	 * @return true if updated successfully
	 */
	@Override
	public boolean updateAccount(Account account) {
		
		Session session = sessionFactory.getCurrentSession();
		session.update(account);
		return true;
	}

	/**
	 * This method is used to display all the accounts inside a database
	 * @return list of accounts
	 */
	@Override
	public List<Account> getAllAccounts() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Account.class);
		return criteria.list();
	}

	
}
