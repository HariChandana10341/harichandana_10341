package com.abc.service;

import java.util.List;

import com.abc.hibernate.entities.Account;
/**
 * This class acts as an interface to account service class which has all the abstract methods of CRUD operations
 * @author CHANDANA
 *
 */
public interface AccountService {

	Account searchAccountByID(int accno);
	
	int insertAccount(Account account);
	
	boolean deleteAccount(int accno);
	
	boolean updateAccount(Account account);
	
	List<Account> getAllAccounts();
}
