package com.abc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.dao.AccountDAO;
import com.abc.hibernate.entities.Account;

/**
 * This class acts as a service class which calls all the methods inside a DAO
 * @author CHANDANA
 *
 */
@Service
public class AccountServieImpl implements AccountService {

	//creating object for accountDAO
	@Autowired
	private AccountDAO accountDAO;
		
	/**
	 * This  method  is used to search account by ID
	 * @param accno
	 * @return account
	 */
	@Transactional
	@Override
	public Account searchAccountByID(int accno) {
		return accountDAO.getAccountByID(accno);
	}

	/**
	 * This method is used to insert account details
	 * @param account
	 * @return account number
	 */
	@Transactional
	@Override
	public int insertAccount(Account account) {
		return accountDAO.insertAccount(account);
	}

	/**
	 * This  method  is used to delete account by ID
	 * @param accno
	 * @return true if deleted successfully
	 */
	@Transactional
	@Override
	public boolean deleteAccount(int accno) {
		return accountDAO.deleteAccount(accno);
	}

	/**
	 * This method is used to update account details
	 * @param account
	 * @return true if updated successfully
	 */
	@Transactional
	@Override
	public boolean updateAccount(Account account) {


		return accountDAO.updateAccount(account);
	}

	/**
	 * This method is used to display all the accounts inside a database
	 * @return list of accounts
	 */
	@Transactional
	@Override
	public List<Account> getAllAccounts() {
		return accountDAO.getAllAccounts();
	}

}
