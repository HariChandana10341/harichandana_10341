package com.retailbillingsystem.provider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.retailbillingsystem.bean.FeedBack;
import com.retailbillingsystem.bean.Invoice_details;
import com.retailbillingsystem.bean.Products;
import com.retailbillingsystem.dao.FeedbackDao;
import com.retailbillingsystem.dao.InvoiceDao;
import com.retailbillingsystem.dao.ProductsDao;
import com.retailbillingsystem.services.BillerServices;
import com.retailbillingsystem.services.StockerServices;

@Path("/biller")
public class BillerProvider {

	@POST
	@Path("/billgenerate")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<String> billGenerate(Products product_array[]) {
		BillerServices billerServices = new BillerServices();
		List<Products> products = Arrays.asList(product_array);
		List<Products> productList = ProductsDao.readFromTable_products();
		ArrayList<String> billingList = new ArrayList<String>();
		ArrayList<Invoice_details> invoice_details = new ArrayList<Invoice_details>();
		StockerServices stockerServices = new StockerServices();
		Invoice_details details = new Invoice_details();
		int invoice_num = 0;
		List<Invoice_details> invoiceDetails = InvoiceDao.readFromTable_invoice();
		// initializing invoice number
		if (invoiceDetails.isEmpty()) {
			invoice_num = 1001;
		} else {
			invoice_num = invoiceDetails.get(invoiceDetails.size() - 1).getInvoiceno();
			invoice_num++;
		}

		double total = 0;
		for (Products product : products) {
			for (Products product1 : productList) {
				if (product.getProductname().equalsIgnoreCase(product1.getProductname())) {
					product.setPrice(billerServices.price_calculation(product.getProductname(), product.getQuantity()));
					total = billerServices.total_calculation(product.getPrice(), total);
					details.setInvoiceno(invoice_num);
					details.setPrice(product.getPrice());
					details.setQuantity(product.getQuantity());
					details.setProduct_id(product1.getProduct_id());
					String temp = stockerServices.updateProductQuantity(product.getProductname(), product.getQuantity(), "2");
					if(temp.equalsIgnoreCase("Product Updated Successfully"))
					{
						billingList.add("Product Name= " + product.getProductname());
						billingList.add("Product Quantity= " + product.getQuantity());
						billingList.add("Product Price= " + product.getPrice());
						invoice_details.add(details);
					}
				}
			}
		}

		double tax = billerServices.tax_caluculation(total);
		double discount = billerServices.discount_calculation(total);

		billerServices.billingProducts(invoice_details);

		billingList.add("Total =" + total);
		billingList.add("Tax= " + tax);
		billingList.add("Discount = " + discount);

		return billingList;
	}

	@POST
	@Path("/feedback")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String customer_feedback(FeedBack feedback) {
		String result = "";
		// creating date object
		Date date1 = new Date();
		BillerServices billerServices = new BillerServices();
		if (!billerServices.mobileCheck(feedback.getMobileNumber())) {
			result = "enter valid mobile number";
		} else {
			// converting the date type into string by using toString
			String date = date1.toString();
			feedback.setDateandtime(date);
			FeedbackDao fd = new FeedbackDao();
			if (fd.feedbackDetails(feedback)) {
				result = "thanks for your feedback";
			} else {
				result = "fail";
			}

		}
		return result;
	}

}
