package com.retailbillingsystem.bean;

import java.util.Date;


/***
 * This class is an object for storing the bill details according to date of sale
 * @author BATCH A
 *
 */
public class Billing_details {
	
	//initializing the variables
	private String invoiceno;
	
	private Date date;
	private double tax;
	private double discount;
	private double total;
	
	/***
	* This method is for getting the tax data of the current bill
	* @return tax
	*/
	public double getTax() {
		return tax;
	}
	
	/***
	* This method is for setting the tax value for the current bill
	* @param tax
	*/
	public void setTax(double tax) {
		this.tax = tax;
	}
	
	/***
	* This method is for getting the discount data of the current bill
	* @return discount
	*/
	public double getDiscount() {
		return discount;
	}
	
	/***
	* This method is for setting the discount value for the current bill
	* @param discount
	*/
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
	/***
	* This method is for getting the total data of the current bill
	* @return total
	*/
	public double getTotal() {
		return total;
	}
	
	/***
	* This method is for setting the total value for the current bill
	* @param total
	*/
	public void setTotal(double total) {
		this.total = total;
	}
	
	/***
	* This method is for getting the invoiceno data of the current bill
	* @return invoiceno
	*/
	public String getInvoiceno() {
		return invoiceno;
	}
	
	/***
	* This method is for setting the invoiceno value for the current bill
	* @param invoiceno
	*/
	public void setInvoiceno(String invoiceno) {
		this.invoiceno = invoiceno;
	}
	
	
	
	/***
	* This method is for getting the date data of the current bill
	* @return date
	*/
	public Date getDate() {
		return date;
	}
	
	/***
	* This method is for setting the date value for the current bill
	* @param date
	*/
	public void setDate(Date date) {
		this.date = date;
	}
}
