<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table border="1">
<tr><th>ID</th><th>NAME</th><th>AGE</th><th>CITY</th><th>STATE</th><tr>
<c:forEach var = "studentDetails" items="${student}" >
<tr>
<td>${studentDetails.id}</td>
<td>${studentDetails.name }</td>
<td>${studentDetails.age }</td>
<td>${studentDetails.address.city }</td>
<td>${studentDetails.address.state }</td>
</tr>
</c:forEach>
</table>
</body>
</html>