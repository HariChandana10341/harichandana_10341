package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class SearchStudent
 */
@WebServlet("/SearchStudent")
public class SearchStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//StudentService object creation
		StudentService service = new StudentServiceImpl();
   		
		//initializing the variables
		int studentId = Integer.parseInt(request.getParameter("Id"));
   		
   		//calling the find by id method to check for the student's existence
   		Student student = service.findById(studentId);
   		
   		RequestDispatcher dispatcher = request.getRequestDispatcher("search_result.jsp");
   		//output validation
   		if(student != null) {

			request.setAttribute("student", student);
			dispatcher.forward(request, response);
		}
		else {
			response.sendRedirect("Error_search.html");
		}
	}

	

}
