package com.sample.servlets;




import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class DisplayAllStudents
 */
@WebServlet("/DisplayAllStudents")
public class DisplayAllStudents extends HttpServlet {
	private static final long serialVersionUID = 1L;
       /**
        * @see HttpServlet #doGet(HttpServletRequest request, HttpServletResponse response)
        */
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   	
   		//StudentService object creation
   		StudentService service = new StudentServiceImpl();
   		
   		//calling fetchAllStudent method to get the list of students
   		List<Student> studentList = service.fetchAllStudents();
   		if(studentList.size() != 0) {
	   		request.setAttribute("student",studentList );
	   		
	   		RequestDispatcher dispatcher = request.getRequestDispatcher("DisplayAll.jsp");
	   		dispatcher.include(request, response);
   	
   		}else {
   			response.sendRedirect("Error.html");
   		}
   		
   		
   	}

}
