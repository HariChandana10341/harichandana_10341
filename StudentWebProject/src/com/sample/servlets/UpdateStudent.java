package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class UpdateStudent
 */
@WebServlet("/UpdateStudent")
public class UpdateStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//initializing the variables
		int studentId = Integer.parseInt(request.getParameter("Id"));
		String studentName = request.getParameter("name");
		String studentAge = request.getParameter("age");
		
		
		
		boolean result = false;
		
		//StudentService object creation
		StudentService service = new StudentServiceImpl();
		
		//print writer creation
		PrintWriter out = response.getWriter();
		
		//calling the find by id method to check for the student's existence
		Student student = service.findById(studentId);
		//student object creation
		Student studentUpdate = new Student();
		//validating the input
		if(student != null) {
			//checking whether to update name or age
			if(studentName.equals("") || studentName.equals(" ") ) {
				studentUpdate.setId(student.getId());
				studentUpdate.setName(student.getName());
				studentUpdate.setAge(Integer.parseInt(studentAge));
				
			}
			else if(studentAge.equals("") || studentAge.equals(" ") ) {
				studentUpdate.setId(student.getId());
				studentUpdate.setName(studentName);
				studentUpdate.setAge(student.getAge());
			}
			else {
				studentUpdate.setId(student.getId());
				studentUpdate.setName(studentName);
				studentUpdate.setAge(Integer.parseInt(studentAge));
				
			}
		
			//calling the update student method 
			result = service.updateStudent(studentUpdate);
	   		
			
			
			//validating the result
			if(result) {
				response.sendRedirect("Update_result.html");
			}
			

		}else {
			response.sendRedirect("Error_update.html");
			 
		}
		
		 
		
	}
	

	}

