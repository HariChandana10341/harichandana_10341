package com.sample.service;

import java.util.List;

import com.sample.bean.Student;
import com.sample.dao.StudentDAO;
import com.sample.dao.StudentDAOImpl;

/**
 * This class implements StudentService interface.It contains all the overridden methods.
 * @author CHANDANA
 *
 */
public class StudentServiceImpl implements StudentService{

	/**
	 * This method is used to call the createStudent method inside StudentDAO method.
	 * 
	 * @return true if student created successfully else false
	 */
	@Override
	public boolean insertStudent(Student student) {
		
		//
		StudentDAO studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.createStudent(student);
		
		return result;
	}

	/**
	 * This method is used to call the SearchById method inside StudentDAO method.
	 * 
	 * @return student object if student found else null
	 */
	@Override
	public Student findById(int id) {
		
		StudentDAO studentDAO = new StudentDAOImpl();
		Student student = studentDAO.searchById(id);
		return student;
	}

	/**
	 * This method is used to call the getAllStudents method inside StudentDAO method.
	 * 
	 * @return List of students
	 */
	@Override
	public List<Student> fetchAllStudents() {
		
		StudentDAO studentDAO = new StudentDAOImpl();
		List<Student> studentList = studentDAO.getAllStudents();
		return studentList;
	}
	
	/**
	 * This method is used to call the deleteStudent method inside StudentDAO method.
	 * 
	 * @return true if student deleted successfully else false
	 */
	public boolean deleteStudent(int id) {
		
		StudentDAO studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.deleteStudent(id);
		
		return result;
		
		
	}
	
	/**
	 * This method is used to call the updateStudent method inside StudentDAO method.
	 * 
	 * @return true if student details updated successfully else false
	 */
	public boolean updateStudent(Student student) {
		
		StudentDAO studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.updateStudent(student);
		
		return result;
		
		
	}
	 

}














