package com.sample.service;

import java.util.List;

import com.sample.bean.Student;

/**
 * This class is an interface which has all the abstract methods which are the services of student object
 * @author CHANDANA
 *
 */
public interface StudentService {
	
	boolean insertStudent( Student student );
	
	Student findById(int id);
	
	List<Student> fetchAllStudents();
	
	boolean deleteStudent(int id);

	boolean updateStudent(Student student);
	
}
