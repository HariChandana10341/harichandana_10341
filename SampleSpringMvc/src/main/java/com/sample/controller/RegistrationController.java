package com.sample.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.sample.bean.RegisterBean;

@Controller
public class RegistrationController {
	
	@GetMapping("/getregistration")
	public String getRegistration()
	{
		return "Registration";
	}
	
	@PostMapping("/registration")
	public String registration(@ModelAttribute RegisterBean bean,ModelMap map)
	{
		return "registration_console";
	}
	
}
