<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<c:if test="${delete eq true }">
<h2>Account deletion Successful</h2>
</c:if>
<c:if test="${delete eq false }">
<h2>Deletion Failed</h2>
</c:if>
</body>
</html>