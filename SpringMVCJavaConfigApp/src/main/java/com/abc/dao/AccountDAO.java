package com.abc.dao;

import java.util.List;

import com.abc.hibernate.entities.Account;

public interface AccountDAO {

	Account getAccountByID(int accno);
	
	boolean insertAccount(Account account);
	
	boolean deleteAccount(int accno);
	
	boolean updateAccount(Account account);
	
	List<Account> getAllAccounts();
}
