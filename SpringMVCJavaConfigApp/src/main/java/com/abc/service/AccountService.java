package com.abc.service;

import java.util.List;

import com.abc.hibernate.entities.Account;

public interface AccountService {

	Account searchAccountByID(int accno);
	
	boolean insertAccount(Account account);
	
	boolean deleteAccount(int accno);
	
	boolean updateAccount(Account account);
	
	List<Account> getAllAccounts();
}
