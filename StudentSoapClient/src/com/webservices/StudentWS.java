/**
 * StudentWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webservices;

public interface StudentWS extends java.rmi.Remote {
    public com.sample.bean.Student[] getAllStudents() throws java.rmi.RemoteException;
    public boolean insertStudent(com.sample.bean.Student student) throws java.rmi.RemoteException;
    public com.sample.bean.Student findById(int id) throws java.rmi.RemoteException;
}
