package com.webservices;

public class StudentWSProxy implements com.webservices.StudentWS {
  private String _endpoint = null;
  private com.webservices.StudentWS studentWS = null;
  
  public StudentWSProxy() {
    _initStudentWSProxy();
  }
  
  public StudentWSProxy(String endpoint) {
    _endpoint = endpoint;
    _initStudentWSProxy();
  }
  
  private void _initStudentWSProxy() {
    try {
      studentWS = (new com.webservices.StudentWSServiceLocator()).getStudentWS();
      if (studentWS != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)studentWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)studentWS)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (studentWS != null)
      ((javax.xml.rpc.Stub)studentWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.webservices.StudentWS getStudentWS() {
    if (studentWS == null)
      _initStudentWSProxy();
    return studentWS;
  }
  
  public com.sample.bean.Student[] getAllStudents() throws java.rmi.RemoteException{
    if (studentWS == null)
      _initStudentWSProxy();
    return studentWS.getAllStudents();
  }
  
  public com.sample.bean.Student findById(int id) throws java.rmi.RemoteException{
    if (studentWS == null)
      _initStudentWSProxy();
    return studentWS.findById(id);
  }
  
  public boolean insertStudent(com.sample.bean.Student student) throws java.rmi.RemoteException{
    if (studentWS == null)
      _initStudentWSProxy();
    return studentWS.insertStudent(student);
  }
  
  
}