package com.sample.servlets;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.webservices.StudentWSProxy;


/**
 * Servlet implementation class DisplayAllStudents
 */
@WebServlet("/DisplayAllStudents")
public class DisplayAllStudents extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * this servlet method is used for displaying and to get the request from the
	 * html page and sends back the response to the user in the form of html
	 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
        StudentWSProxy studentWSProxy = new StudentWSProxy();
        
		List<Student> studentList = Arrays.asList(studentWSProxy.getAllStudents());
		request.setAttribute("studentList", studentList);
		RequestDispatcher rd = request.getRequestDispatcher("display_all.jsp");
		rd.forward(request, response);
	}

}
