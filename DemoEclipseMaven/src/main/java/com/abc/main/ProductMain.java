package com.abc.main;

import com.abc.entities.Product;
import com.abc.service.ProductService;

public class ProductMain {

	public static void main(String[] args) {
		
		Product product = new Product();
		product.setProductId("P101");
		product.setProductName("VIVO");
		product.setProductPrice(9500);
		product.setCategory("Mobiles");
		
		ProductService service = new ProductService();
		service.insertProduct(product);
		System.out.println("Done");
		
	}
}
