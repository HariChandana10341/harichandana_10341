package com.abc.DAO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.abc.entities.Product;
import com.abc.util.HibernateUtil;

public class ProductDAO {

	public boolean createProduct(Product product) {
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
				
		Transaction txn = session.beginTransaction();
		
		session.save(product);
		System.out.println("product saved");
		txn.commit();
		
		session.close();
		return true;
		}
}
