package com.abc.service;

import com.abc.DAO.ProductDAO;
import com.abc.entities.Product;

public class ProductService {

	ProductDAO productDAO;
	
	public ProductService() {
		productDAO = new ProductDAO();
	}
	public boolean insertProduct(Product product) {
		
		boolean flag = productDAO.createProduct(product);
		
		return flag;
	}
}

