

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Display
 */
@WebServlet("/Display")
public class Display extends HttpServlet {
	private static final long serialVersionUID = 1L;
    

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//creating a session which uses the existing session
		HttpSession session = request.getSession(false);

		//adding the input to the list
		List<String> hobbies = (List<String>) session.getAttribute("hobbies");
		
		//creating a print writer
		PrintWriter printWriter = response.getWriter();
		
		//traversing the list for printing the data
		for(String string : hobbies)
		{
			printWriter.println(string);
		}
		//using session timeout condition. if input is given after the time out then the data is lost
		session.setMaxInactiveInterval(10);
	
		printWriter.println("<a href=\"shop.html\">go back to previous page</a>");
	}

}
