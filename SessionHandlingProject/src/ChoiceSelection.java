

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Login
 */
@WebServlet("/ChoiceSelection")
public class ChoiceSelection extends HttpServlet {
	private static final long serialVersionUID = 1L;
	List<String> string;
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//using the existing session
		HttpSession session = request.getSession(false);

		//if session not exists then creating a new session 
		if(session == null)
		{
			session = request.getSession();
			//creating a new list
			string=new ArrayList<String>();
			
		}
		else
		{
			//adding the input to a list
			string = (List<String>) session.getAttribute("hobbies");
		}
		//getting the input into a string array
		String hobbise [] = request.getParameterValues("hobbies");
		
		//adding the each input into list by traversing
		for(String hobby:hobbise) {
			string.add(hobby);
		}
//		System.out.println(session);
		session.setAttribute("hobbies", string);
		
		//creating a request dispatcher for including the response and request
		RequestDispatcher dispatcher = request.getRequestDispatcher("/Display");
		dispatcher.include(request, response);
		
	}

}
