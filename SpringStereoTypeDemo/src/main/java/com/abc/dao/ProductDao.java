package com.abc.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.SharedSessionContract;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.abc.entity.Product;
import com.abc.util.HibernateUtil;

@Repository
public class ProductDao {
	
	public boolean saveProduct(Product product)
	{
		//Taking session factory object
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		//saving product object into database
		session.save(product);
		
		Transaction transaction =session.beginTransaction();
		transaction.commit();
		
		//closing resources
		session.close();
		return true;
	}
	
}