package com.springdemo.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import com.springdemo.bean.EmployeeBean;
import com.springdemoannotations.javaconfiguration.ApplicationConfiguration;

public class SpringDemoMain {
	
	public static void main(String[] args) {
		
		//Creating object for application cotext for fetching object using configuration class
		ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
		//Getting the object
		EmployeeBean employeeBean = (EmployeeBean) context.getBean(EmployeeBean.class); 
		System.out.println("ID: " + employeeBean.getEmpId());
		System.out.println("Name: " + employeeBean.getEmpName());
		System.out.println("City: " + employeeBean.getEmpAddress().getCity());
		System.out.println("State: " + employeeBean.getEmpAddress().getState());
		((AbstractApplicationContext) context).close();
	}

}