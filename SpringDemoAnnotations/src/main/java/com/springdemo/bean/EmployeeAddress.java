package com.springdemo.bean;

public class EmployeeAddress {
	//declaring fields required for the bean class
	private String city;
	private String state;
	
	//Getters and setters for the fields
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
}