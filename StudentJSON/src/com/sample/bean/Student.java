package com.sample.bean;


/**
 * This class creates student object which is useful to store all the details of each student.
 * @author CHANDANA
 *
 */
public class Student {

	//variable initialization
	private int id;
	private  String name;
	private int age;
	private Address address;
	
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * This method used to get Id of the particular student
	 * @return id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * This method used to set Id of the particular student when required
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * This method used to get name of the particular student
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * This method used to set name of the particular student when required
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 *  This method used to get age of the particular student
	 * @return age
	 */
	public int getAge() {
		return age;
	}
	
	/**
	 * This method used to set age of the particular student when required
	 * @param age
	 */
	public void setAge(int age) {
		this.age = age;
	}
	
	
	
}
