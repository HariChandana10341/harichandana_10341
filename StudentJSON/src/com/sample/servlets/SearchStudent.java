package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class SearchStudent
 */
@WebServlet("/SearchStudent")
public class SearchStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson = new Gson();  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//StudentService object creation
		StudentService service = new StudentServiceImpl();
   		
		//initializing the variables
		int studentId = Integer.parseInt(request.getParameter("Id"));
		PrintWriter out = response.getWriter();
		
		//setting json content type
		response.setContentType("application/json");
		
		response.setCharacterEncoding("UTF-8");
		
		

		// calling findbyId method of service classs
		Student student = service.findById(studentId);
		
		String studentJsonString = this.gson.toJson(student);

		// writting object into stream
		out.print(studentJsonString);
		
		out.flush();

	}
}


