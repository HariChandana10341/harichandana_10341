package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class DeleteStudent
 */
@WebServlet("/DeleteStudent")
public class DeleteStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson = new Gson();    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//StudentService object creation
		StudentService service = new StudentServiceImpl();
   		
		//initializing the variables
		boolean result = false;
		
   		int studentId = Integer.parseInt(request.getParameter("Id"));
   		

   		PrintWriter out = response.getWriter();
   		
   			//setting json content type
   			response.setContentType("application/json");
   			
   			response.setCharacterEncoding("UTF-8");
   		//calling find by id method to find the student's existence
   		Student student = service.findById(studentId);
   		
   		//checking the condition for deleting student
   		if(student != null) {
   			result = service.deleteStudent(studentId);
   		
   		
		
			
			
			if(result) {
				
	   			String studentJsonString = this.gson.toJson(student);

	   			// writting object into stream
	   			out.print(studentJsonString);
			}
			
   		}else {
   			
   			response.sendRedirect("Error_delete.html");
   			
   			
   		}
   		out.flush();
		
	}

}
