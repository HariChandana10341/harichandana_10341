package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.sample.bean.Address;
import com.sample.bean.Student;
import com.sample.dao.StudentDAO;
import com.sample.dao.StudentDAOImpl;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class StudentServlet
 */
@WebServlet("/InsertStudent")
public class InsertStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson = new Gson();
  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//intializing the variables
		String studentName = request.getParameter("name");
		int studentAge = Integer.parseInt(request.getParameter("age"));
		String studentCity = request.getParameter("city");
		String studentState = request.getParameter("state");	
		
		
        //StudentService object creation
		StudentService service = new StudentServiceImpl();
		
		
		
			//student object creation
			Student student = new Student();
			//setting the values into object
			student.setName(studentName);
	        student.setAge(studentAge);
	        Address address = new Address();
	        address.setCity(studentCity);
	        address.setState(studentState);
	        student.setAddress(address);
	        
	        //calling the inserting student method
			boolean result = service.insertStudent(student);		
			

	   		PrintWriter out = response.getWriter();
	   		
	   			//setting json content type
	   			response.setContentType("application/json");
	   			
	   			response.setCharacterEncoding("UTF-8");
	   			
			
			
			//validating the result
			if(result) {
				
				String studentJsonString = this.gson.toJson(student);

	   			// writting object into stream
	   			out.print(studentJsonString);
				 
				
				
			}else {
				//out.print("Student ID already exists. Enter another Id");
				response.sendRedirect("Error_insert.html");
				
				
			}
			
			
		
		
		
	}

}
