package com.sample.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.sample.bean.Address;
import com.sample.bean.Student;
import com.sample.util.DBUtil;

/**
 * This method is used to do all the DataBase operations which affects the student data. This method implements StudentDAO.
 * @author CHANDANA
 *
 */
public class StudentDAOImpl implements StudentDAO {

	/**
	 * This method is used to create new student.This method overrides createStudent method from  StudentDAO interface.
	 * 
	 * @return true if student created successfully else false
	 */
	@Override
	public boolean createStudent(Student student) {
		
		//variable declaration
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		
		//query for inserting Student details into DB
		String sql = "insert into student_tbl values( ?, ?, ?, ?, ?)";
		try {
			
			//calling DBUtil method for connection establishment
			con = DBUtil.getCon();
			//setting the values using prepared statement
			ps = con.prepareStatement(sql);
			ps.setInt(1, student.getId());
			ps.setString(2, student.getName());
			ps.setInt(3, student.getAge());
			ps.setString(4, student.getAddress().getCity());
			ps.setString(5,student.getAddress().getState());
			
			//storing value of no of rows effected
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				result = true;
			} 
			
			
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}

	/**
	 * This method is used to search student by taking his ID.This method overrides createStudent method from  StudentDAO interface.
	 * 
	 *  @return Student object if student exists else null
	 */
	@Override
	public Student searchById(int id) {

		//variable declaration
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Student st = null;
		
		//query for searching Student details 
		String sql = "select * from student_tbl where id = ?";
		try {
			//calling DBUtil method for connection establishment
			con = DBUtil.getCon();
			
			//setting the values using prepared statement
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			
			while(rs.next()) {
				
				Address address = new Address();
				address.setCity(rs.getString(4));
				address.setState(rs.getString(5));
				
				
				st= new Student();
				st.setAddress(address);
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
				
				
			}
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return st;
	}

	/**
	 * This method is used to get details of each student.This method overrides createStudent method from  StudentDAO interface.
	 * 
	 * @return List of students
	 */
	@Override
	public List<Student> getAllStudents() {

		//variable declaration
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Student st = null;
		List <Student> studentList = new ArrayList<Student>();
		
		//query for displaying Student details
		String sql = "select * from student_tbl ";
		try {
			//calling DBUtil method for connection establishment
			con = DBUtil.getCon();
			
			//setting the values using prepared statement
			stmt= con.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				
				Address address = new Address();
				address.setCity(rs.getString(4));
				address.setState(rs.getString(5));
				
				
				st= new Student();
				st.setAddress(address);
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
				studentList.add(st);
			}
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return studentList;
	}
	
	/**
	 * This method is used to delete student by taking Id of particular student.This method overrides createStudent method from  StudentDAO interface
	 *@return true student deleted successfully else false
	 */
	@Override
	public boolean deleteStudent(int id) {

		//variable declaration
		Connection con = null;
		PreparedStatement ps = null;
		
		//query for deleting Student details from DB
		String sql = "delete from student_tbl where id = ?";
		try {
			//calling DBUtil method for connection establishment
			con = DBUtil.getCon();
			
			//setting the values using prepared statement
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			//storing value of no of rows effected
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				return true;
			} 
			
			
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return false;
	}
	/**
	 * This method is used to update student details.This method overrides createStudent method from  StudentDAO interface.
	 * 
	 * @return true if updated successfully else false 
	 */
	public boolean updateStudent(Student student) {

		//variable declaration
		Connection con = null;
		PreparedStatement ps = null;
		
		
		//query for updating Student details into DB
		String sql = "update student_tbl set name = ?,age = ? where id = ?";
		try {
			//calling DBUtil method for connection establishment
			con = DBUtil.getCon();
			
			//setting the values using prepared statement
			ps = con.prepareStatement(sql);
			ps.setString(1, student.getName());
			ps.setInt(2, student.getAge());
			ps.setInt(3, student.getId());
//			ps.setString(4, student.getAddress().getCity());
//			ps.setString(5,student.getAddress().getState());
			//storing value of no of rows effected
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				return true;
			} 
			
			
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return false;
	} 

}











