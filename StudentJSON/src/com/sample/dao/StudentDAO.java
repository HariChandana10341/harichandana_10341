package com.sample.dao;



import java.util.List;

import com.sample.bean.Student;

/**
 * This is an interface which contains all the abstract methods which are used to perform different operations.
 * @author CHANDANA
 *
 */
public interface StudentDAO {
	
	boolean createStudent( Student student );
	
	Student searchById(int id);
	
	List<Student> getAllStudents();
	
	boolean deleteStudent(int id);

	boolean updateStudent(Student student);
}
